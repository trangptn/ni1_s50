const chai=require("chai");
const chaiHttp = require("chai-http");
const server=require('../index');
const mongoose  = require("mongoose");
const should=chai.should();

chai.use(chaiHttp);

describe("MongoDb connection test",()=>{
    it("Should connect to MongoDb",(done)=>{
        chai.request(server)
        .get("/test/mongo-connection")
        .end((err,res)=>{
            res.should.have.status(200);
            res.body.should.have.property("message").eql("MongoDb connected successfully");
            done();
        })
    })
})

describe("POST create order",()=>{
    it("Should create a new order",(done)=>{
        const newOrder={
            reqOrdercode:"pizza02",
            reqPizzaSize:"22cm",
            reqPizzaType:"Y",
            reqStatus:"Dang giao",
            userId:"662f3a0f3c30a63e90384c66"
        }
        chai.request(server)
        .post('/devcamp-pizza365/orders')
        .send(newOrder)
        .end((err,res)=>{
            console.log(err);
            chai.expect(res).to.have.status(201);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.have.property('message').eql("Tao order thanh cong");
            chai.expect(res.body).to.have.property('data');
            chai.expect(res.body.data).to.have.property("orderCode").eql(newOrder.reqOrdercode);
            chai.expect(res.body.data).to.have.property("pizzaSize").eql(newOrder.reqPizzaSize);
            chai.expect(res.body.data).to.have.property("pizzaType").eql(newOrder.reqPizzaType);
            chai.expect(res.body.data).to.have.property("status").eql(newOrder.reqStatus);
            done();
        })
       
    })
})

describe("Get all orders",()=>{
    it("It should get all orders of database",(done)=>{
        chai.request(server)
        .get("/devcamp-pizza365/orders")
        .end((err,res)=>{
            res.should.have.status(200);
            done();
        })

    })
})

describe("Get order by Id",()=>{
    it("should get a oredr by valid ID",(done)=>{
        const orderId= new mongoose.Types.ObjectId();
        chai.request(server)
        .get(`/devcamp-pizza365/orders/${orderId}`)
        .end((err,res)=>{
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            chai.expect(res.body).to.have.property("message").eql("Lay thong tin order thanh cong");
            chai.expect(res.body).to.have.property("data");
            done();
        })
    })
})

describe("Put order by Id",()=>{
    it("should update a order by Valid Id",(done)=>{
        const orderId="662f4acf73588562c5649194";
        const updatOrder={
            reqPizzaSize:"18cm",
            reqPizzaType:"truyen thong",
            reqStatus:"da giao"
        }
        chai.request(server)
        .put(`/devcamp-pizza365/orders/${orderId}`)
        .send(updatOrder)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an("object");
                chai.expect(res.body).to.have.property("message").eql("Cap nhat thong tin order thanh cong");
                if(res.body.hasOwnProperty("data") && res.body.data !==null)
                {
                    const updateOrder=res.body.data;
                    chai.expect(updateOrder).to.have.property("_id").equal(orderId.toString());
                   
                }
                else{
                    throw new Error("Response does not contain a valid 'order' property ")
                }
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})

describe("Delete order by Id",()=>{
    it("should delete a order by valid ID",(done)=>{
        const orderId="662f4acf73588562c5649194";
        chai.request(server)
        .delete(`/devcamp-pizza365/orders/${orderId}`)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an('object');
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})