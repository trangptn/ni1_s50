const chai=require("chai");
const chaiHttp = require("chai-http");
const server=require('../index');
const mongoose  = require("mongoose");
const should=chai.should();

chai.use(chaiHttp);

describe("MongoDn connection test",()=>{
    it("Should connect to MongoDb",(done)=>{
        chai.request(server)
        .get("/test/mongo-connection")
        .end((err,res)=>{
            res.should.have.status(200);
            res.body.should.have.property("message").eql("MongoDb connected successfully");
            done();
        })
    })
})

describe("POST create drinks",()=>{
    it("Should create a new drink",(done)=>{
        const newDink={
            manuocuong:"PEPSI",
            tennuocuong:"PEPSI",
            dongia:25000
        }
        chai.request(server)
        .post('/devcamp-pizza365/drinks')
        .send(newDink)
        .end((err,res)=>{
            chai.expect(res).to.have.status(201);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.have.property('message').eql("Tao drink thanh cong");
            chai.expect(res.body).to.have.property('data');
            chai.expect(res.body.data).to.have.property('maNuocUong').eql(newDink.manuocuong);
            chai.expect(res.body.data).to.have.property("tenNuocUong").eql(newDink.tennuocuong);
            chai.expect(res.body.data).to.have.property("donGia").eql(newDink.dongia);
        })
        done();
    })
})

describe("Get all drinks",()=>{
    it("It should get all drink of database",(done)=>{
        chai.request(server)
        .get("/devcamp-pizza365/drinks")
        .end((err,res)=>{
            res.should.have.status(200);
            done();
        })

    })
})

describe("Get drink by Id",()=>{
    it("should get a drink by valid ID",(done)=>{
        const drinkid= new mongoose.Types.ObjectId();
        chai.request(server)
        .get(`/devcamp-pizza365/drinks/${drinkid}`)
        .end((err,res)=>{
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            chai.expect(res.body).to.have.property("message").eql("Lay thong tin drink thanh cong");
            chai.expect(res.body).to.have.property("data");
            done();
        })
    })
})

describe("Put drink by Id",()=>{
    it("should update a drink by Valid Id",(done)=>{
        const drinkid="662f333a033f9dd9b91473ce";
        const updatDrink={
            manuocuong:"updatePEPSI",
            tennuocuong:"updatePEPSI",
            dongia:30000
        }
        chai.request(server)
        .put(`/devcamp-pizza365/drinks/${drinkid}`)
        .send(updatDrink)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an("object");
                chai.expect(res.body).to.have.property("message").eql("Update thong tin drink thanh cong");
                if(res.body.hasOwnProperty("data") && res.body.data !==null)
                {
                    const updateDrink=res.body.data;
                    chai.expect(updateDrink).to.have.property("_id").equal(drinkid.toString());
                    // chai.expect(updateDrink).to.have.property("maNuocUong").equal(updatDrink.manuocuong);
                    // chai.expect(updateDrink).to.have.property("tenNuocUong").equal(updatDrink.tennuocuong);
                    // chai.expect(updateDrink).to.have.property("donGia").equal(updatDrink.dongia);
                }
                else{
                    throw new Error("Response does not contain a valid 'drink' property ")
                }
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})

describe("Delete drink by Id",()=>{
    it("should delete a drink by valid ID",(done)=>{
        const drinkid="662f333a033f9dd9b91473ce";
        chai.request(server)
        .delete(`/devcamp-pizza365/drinks/${drinkid}`)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an('object');
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})