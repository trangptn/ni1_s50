const chai=require("chai");
const chaiHttp = require("chai-http");
const server=require('../index');
const mongoose  = require("mongoose");
const should=chai.should();

chai.use(chaiHttp);

describe("MongoDb connection test",()=>{
    it("Should connect to MongoDb",(done)=>{
        chai.request(server)
        .get("/test/mongo-connection")
        .end((err,res)=>{
            res.should.have.status(200);
            res.body.should.have.property("message").eql("MongoDb connected successfully");
            done();
        })
    })
})

describe("POST create user",()=>{
    it("Should create a new user",(done)=>{
        const newUser={
            reqFullname:"phung thi ngoc",
            reqEmail:"ngoc@gmail.com",
            reqAddress:"80 Cao Lo",
            reqPhone:"0935342222"
        }
        chai.request(server)
        .post('/devcamp-pizza365/users')
        .send(newUser)
        .end((err,res)=>{
            chai.expect(res).to.have.status(201);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.have.property('message').eql("Tao user thanh cong");
            chai.expect(res.body).to.have.property('data');
            chai.expect(res.body.data).to.have.property("fullName").eql(newUser.reqFullname);
            chai.expect(res.body.data).to.have.property("email").eql(newUser.reqEmail);
            chai.expect(res.body.data).to.have.property("address").eql(newUser.reqAddress);
            chai.expect(res.body.data).to.have.property("phone").eql(newUser.reqPhone);
            done();
        })
       
    })
})

describe("Get all users",()=>{
    it("It should get all users of database",(done)=>{
        chai.request(server)
        .get("/devcamp-pizza365/users")
        .end((err,res)=>{
            res.should.have.status(200);
            done();
        })

    })
})

describe("Get user by Id",()=>{
    it("should get a drink by valid ID",(done)=>{
        const userid= new mongoose.Types.ObjectId();
        chai.request(server)
        .get(`/devcamp-pizza365/users/${userid}`)
        .end((err,res)=>{
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            chai.expect(res.body).to.have.property("message").eql("Lay thong tin user thanh cong");
            chai.expect(res.body).to.have.property("data");
            done();
        })
    })
})

describe("Put user by Id",()=>{
    it("should update a voucher by Valid Id",(done)=>{
        const userid="662f38f54dc04c6b99cef2dc";
        const updatUser={
            fullName:"Ngoc Trang",
            address:"180 Cao Lo"
        }
        chai.request(server)
        .put(`/devcamp-pizza365/users/${userid}`)
        .send(updatUser)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an("object");
                chai.expect(res.body).to.have.property("message").eql("Update thong tin user thanh cong");
                if(res.body.hasOwnProperty("data") && res.body.data !==null)
                {
                    const updateUser=res.body.data;
                    chai.expect(updateUser).to.have.property("_id").equal(userid.toString());
                   
                }
                else{
                    throw new Error("Response does not contain a valid 'voucher' property ")
                }
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})

describe("Delete user by Id",()=>{
    it("should delete a user by valid ID",(done)=>{
        const userid="662f38f54dc04c6b99cef2dc";
        chai.request(server)
        .delete(`/devcamp-pizza365/users/${userid}`)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an('object');
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})