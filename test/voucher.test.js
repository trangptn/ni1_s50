const chai=require("chai");
const chaiHttp = require("chai-http");
const server=require('../index');
const mongoose  = require("mongoose");
const should=chai.should();

chai.use(chaiHttp);

describe("MongoDb connection test",()=>{
    it("Should connect to MongoDb",(done)=>{
        chai.request(server)
        .get("/test/mongo-connection")
        .end((err,res)=>{
            res.should.have.status(200);
            res.body.should.have.property("message").eql("MongoDb connected successfully");
            done();
        })
    })
})

describe("POST create voucher",()=>{
    it("Should create a new voucher",(done)=>{
        const newVoucher={
            mavoucher:"VC30",
            phantramgiamgia:30,
            ghichu:"Giam 30 phan tram"
        }
        chai.request(server)
        .post('/devcamp-pizza365/vouchers')
        .send(newVoucher)
        .end((err,res)=>{
            chai.expect(res).to.have.status(201);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.have.property('message').eql("Tao voucher thanh cong");
            chai.expect(res.body).to.have.property('data');
            chai.expect(res.body.data).to.have.property('maVoucher').eql(newVoucher.mavoucher);
            chai.expect(res.body.data).to.have.property("phanTramGiamGia").eql(newVoucher.phantramgiamgia);
            chai.expect(res.body.data).to.have.property("ghiChu").eql(newVoucher.ghichu);
        })
        done();
    })
})

describe("Get all vouchers",()=>{
    it("It should get all voucher of database",(done)=>{
        chai.request(server)
        .get("/devcamp-pizza365/vouchers")
        .end((err,res)=>{
            res.should.have.status(200);
            done();
        })

    })
})

describe("Get voucher by Id",()=>{
    it("should get a drink by valid ID",(done)=>{
        const voucherid= new mongoose.Types.ObjectId();
        chai.request(server)
        .get(`/devcamp-pizza365/vouchers/${voucherid}`)
        .end((err,res)=>{
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            chai.expect(res.body).to.have.property("message").eql("Lay thong tin voucher thanh cong");
            chai.expect(res.body).to.have.property("data");
            done();
        })
    })
})

describe("Put voucher by Id",()=>{
    it("should update a voucher by Valid Id",(done)=>{
        const voucherid="662f33ebeac6831ce3691033";
        const updatVoucher={
            phantramgiamgia:15,
            ghichu:"Giam 15 phan tram"
        }
        chai.request(server)
        .put(`/devcamp-pizza365/vouchers/${voucherid}`)
        .send(updatVoucher)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an("object");
                chai.expect(res.body).to.have.property("message").eql("Update thong tin voucher thanh cong");
                if(res.body.hasOwnProperty("data") && res.body.data !==null)
                {
                    const updateVoucher=res.body.data;
                    chai.expect(updateVoucher).to.have.property("_id").equal(voucherid.toString());
                    // chai.expect(updateDrink).to.have.property("maNuocUong").equal(updatDrink.manuocuong);
                    // chai.expect(updateDrink).to.have.property("tenNuocUong").equal(updatDrink.tennuocuong);
                    // chai.expect(updateDrink).to.have.property("donGia").equal(updatDrink.dongia);
                }
                else{
                    throw new Error("Response does not contain a valid 'voucher' property ")
                }
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})

describe("Delete voucher by Id",()=>{
    it("should delete a voucher by valid ID",(done)=>{
        const voucherid="662f33ebeac6831ce3691033";
        chai.request(server)
        .delete(`/devcamp-pizza365/vouchers/${voucherid}`)
        .end((err,res)=>{
            try{
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an('object');
                done();
            }
            catch(err)
            {
                done(err);
            }
        })
    })
})