//import thu vien express
const express = require('express');

const mongoose=require('mongoose');
const port= 8000;

const app= new express();

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
.then(() =>{
    console.log("Connect mongoDB Successfully");
})
.catch((error) =>{
    console.log(error);
});

app.get("/test/mongo-connection",(req,res)=>{
    res.json({
        message:"MongoDb connected successfully"
    })
})
app.use(express.json());

const voucherRouter=require("./app/router/voucherRouter");
const orderRouter=require("./app/router/orderRouter");
const drinkRouter=require("./app/router/drinkRouter");
const userRouter=require("./app/router/userRouter");



app.use("/",voucherRouter);
app.use("/",orderRouter);
app.use("/",drinkRouter);
app.use("/",userRouter);

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})

module.exports=app;