const mongoose=require("mongoose");
const schema= mongoose.Schema;
const newVoucherSchema=new schema({
    __id:mongoose.Types.ObjectId,
    maVoucher:{
        type:String,
        unique:true,
        required:true
    },
    phanTramGiamGia:{
        type:Number,
        required:true
    },
    ghiChu:{
        type:String,
        required:false
    }
},
{
    timestamps:true
})
module.exports=mongoose.model("voucher",newVoucherSchema);