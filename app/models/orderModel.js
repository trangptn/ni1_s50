const mongoose = require("mongoose");

const Schema= mongoose.Schema;

const orderSchema= new Schema({
    __id: mongoose.Types.ObjectId,
    orderCode:
    {
        type:String,
        required:true
    },
    pizzaType: {
       type:String,
       required:true
    },
    pizzaSize:{
        type:String,
        required:true
    },
    voucher:[
        {
            type:mongoose.Types.ObjectId,
            ref:"voucher"
        }
    ],
    drink: [
        {
            type:mongoose.Types.ObjectId,
            ref:"drink"
        }
    ],
    status: {
        type:String,
        required:true
    }
},
{
    timestamps:true
}
);

module.exports=mongoose.model("order",orderSchema);