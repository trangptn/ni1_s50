const express=require('express');
const router=express.Router();

const path=require('path');

router.use(express.static(__dirname+'/../views'));

const {
    createOrderMiddleware,
    getAllOrderMiddleware,
    getOrderByIdMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}=require("../middlewares/orderMiddleware");

const{
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    // orderHandle
}=require("../controllers/orderController");

router.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+ '/../views/sample.06restAPI.order.pizza365.v2.0.html'));
})
router.post("/devcamp-pizza365/orders",createOrderMiddleware,createOrder);
router.get("/devcamp-pizza365/orders",getAllOrderMiddleware,getAllOrder);
router.get("/devcamp-pizza365/orders/:orderId",getOrderByIdMiddleware,getOrderById);
router.put("/devcamp-pizza365/orders/:orderId",updateOrderMiddleware,updateOrderById);
router.delete("/devcamp-pizza365/orders/:orderId",deleteOrderMiddleware,deleteOrderById);
// router.post("/devcamp-pizza365/orders",orderHandle);
module.exports=router;