const express=require('express');
const router=express.Router();
const{
    createDrink,
    getAllDrinks,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById,
    // orderHandle
}=require("../controllers/drinkController");
router.post("/devcamp-pizza365/drinks",createDrink);
router.get("/devcamp-pizza365/drinks",getAllDrinks);
router.get("/devcamp-pizza365/drinks/:drinkid",getDrinkById);
router.put("/devcamp-pizza365/drinks/:drinkid",updateDrinkById);
router.delete("/devcamp-pizza365/drinks/:drinkid",deleteDrinkById);
// router.post("/devcamp-pizza365/orders",orderHandle);
module.exports=router;