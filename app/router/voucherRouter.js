const express=require('express');
const router=express.Router();
const{
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById,
    // orderHandle
}=require("../controllers/voucherController");

router.post("/devcamp-pizza365/vouchers",createVoucher);
router.get("/devcamp-pizza365/vouchers",getAllVoucher);
router.get("/devcamp-pizza365/vouchers/:voucherid",getVoucherById);
router.put("/devcamp-pizza365/vouchers/:voucherid",updateVoucherById);
router.delete("/devcamp-pizza365/vouchers/:voucherid",deleteVoucherById);
// router.post("/devcamp-pizza365/orders",orderHandle);
module.exports=router;