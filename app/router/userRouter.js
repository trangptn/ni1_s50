const express=require('express');
const router=express.Router();

const {
    createUserMiddleware,
    getAllUserMiddleware,
    getUserByIdMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}=require("../middlewares/userMiddleware");

const{
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
}=require("../controllers/userController");

router.post("/devcamp-pizza365/users",createUser);
router.get("/devcamp-pizza365/users",getAllUsers);
router.get("/devcamp-pizza365/users/:userid",getUserById);
router.put("/devcamp-pizza365/users/:userid",updateUserById);
router.delete("/devcamp-pizza365/users/:userid",deleteUserById);
module.exports=router;