const mongoose = require('mongoose');
const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');
// const userModel = require('../models/userModel');

const createOrder = async (req, res) => {
    //B1:Thu thap du lieu
    const {
        reqOrdercode,
        reqPizzaSize,
        reqPizzaType,
        reqStatus,
        userId
     } = req.body;
    //B2:Validate data
    if (!reqOrdercode) {
        res.status(400).json({
            message: "Order code khong hop le"
        })
        return false;
    }
    if (!reqPizzaSize) {
        res.status(400).json({
            message: "Pizza Size khong hop le"
        })
        return false;
    }
    if (!reqPizzaType) {
        res.status(400).json({
            message: "Pizza Type khong hop le"
        })
        return false;
    }
    if (!reqStatus) {
        res.status(400).json({
            message: "Status khong hop le"
        })
        return false;
    }
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        res.status(400).json({
            message: "User Id khong hop le"
        })
        return false;
    }
    //B3:Xu ly va tra ve ket qua
    try {
        // B3: Xu ly du lieu

        let newOrder ={
            orderCode:reqOrdercode,
            pizzaSize:reqPizzaSize,
            pizzaType:reqPizzaType,
            status:reqStatus,
        }
        const result = await orderModel.create(newOrder);
        const savedUser = await userModel.findByIdAndUpdate(userId, {
                $push: {
                    orders: result._id
                }
            })
        return res.status(201).json({
            message: "Tao order thanh cong",
            data: result
        })
        
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra"

        })
    }
}

const getAllOrder=async (req,res) =>{
    //B1: Thu thap du lieu
    //B2:validate data
    //B3: Xu ly va tra ve ket qua
    try {
        const result = await orderModel.find();

        return res.status(200).json({
            message: "Lay danh sach order thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getOrderById=async (req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id khong hop le!"
       })
   }
   //B3: Xu ly va tra ve ket qua
   try {
    const result = await orderModel.findById(orderId);

    return res.status(200).json({
        message: "Lay thong tin order thanh cong",
        data: result
    })
} catch (error) {
return res.status(500).json({
    message: "Co loi xay ra"
})
}
}
const updateOrderById= async (req,res)=>{
    //B1: Thu thap du lieu
    var orderId=req.params.orderId;
    const {
        reqPizzaSize,
        reqPizzaType,
        reqStatus,
        } = req.body;
   //B2:validate data
   if(!mongoose.Types.ObjectId.isValid(orderId))
   {
       return res.status(400).json({
           message:"Id khong hop le!"
       })
   }
   if (reqPizzaSize=="") {
    res.status(400).json({
        message: "Pizza Size khong hop le"
    })
    return false;
}
if (reqPizzaType=="") {
    res.status(400).json({
        message: "Pizza Type khong hop le"
    })
    return false;
}
if (reqStatus=="") {
    res.status(400).json({
        message: "Status khong hop le"
    })
    return false;
}
   //B3: Xu ly va tra ve ket qua
  try{
    let updateOrder={
        pizzaSize:reqPizzaSize,
        pizzaType:reqPizzaType,
        status:reqStatus,
    }
    const updateO= await orderModel.findByIdAndUpdate(orderId,updateOrder);
    if(updateO){
        return res.status(200).json({
            message:"Cap nhat thong tin order thanh cong",
            data: updateO
    })
  }
  else{
        return res.status(404).json({
        message:"Khong tim thay thong tin order"
        })
  }
}catch(err){
           return res.status(500).json({
               message:`Co loi xay ra: ${err.message}`
           })
       }
}
const deleteOrderById= async (req,res)=>{
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    var userId=req.query.userId;


    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Order ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const deleteOrder= await  orderModel.findByIdAndDelete(orderId)
        if(userId != undefined){
          await  userModel.findByIdAndUpdate(userId,{
                $pull: {orders: orderId}})
        }

        if(deleteOrder){
            return res.status(200).json({
                message:"Xoa thong tin order thanh cong",
                data: deleteOrder
               })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin order"
            })
        }
    } catch (error) {
        return res.status(500).json({
        message:`Co loi xay ra: ${error.message}`
        })
    }
}

// const orderHandle=(req,res)=>{
//    const email=req.body.email;
//    const random=Math.floor(Math.random()*100);
//    userModel.findOne({email:email})
//    .then((exitEmail)=>{
//         if(exitEmail)
//         {
//             orderModel.create({
//                 _id: new mongoose.Types.ObjectId(),
//                 orderCode:random,
//                 pizzaSize:" ",
//                 pizzaType:" ",
//                 status:" "
//             })
//             .then((createOrder)=>{
//                 res.status(201).json({
//                     data:createOrder
//                 })
//             })
//             .catch((errCreated)=>{
//                 console.log(errCreated);
//                 res.status(500).json({
//                     message:"Loi tao order"
//                 })
//             })
//         }
//         else{
//             userModel.create({
//                 _id: new mongoose.Types.ObjectId(),
//                 email:email,
//                 fullName:" ",
//                 address:" ",
//                 phone:" "
//             })
//             .then((userCreat)=>{
//                 orderModel.create({
//                     _id:userCreat._id,
//                     orderCode:random,
//                     pizzaSize:" ",
//                     pizzaType:" ",
//                     status:" "
//                 })
//                 .then(()=>{
//                     res.status(201).json({
//                         data:userCreat
//                     })
//                 })
//                 .catch((errOrder)=>{
//                     res.status(500).json({
//                         message:"Loi tao order"
//                     })
//                 })
//             })
//             .catch((errUser)=>{
//                 res.status(500).json({
//                     message:"Loi tao user"
//                 })
//             })
//         }
//    })
//    .catch((err)=>{
//     console.log(err);
//     res.status(500).json({
//         message:"Loi "
//     })
//    })
// }
module.exports={
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    // orderHandle
}