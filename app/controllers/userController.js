const userModel = require('../models/userModel');

const mongoose = require('mongoose');

const createUser= async (req, res) => {
    // B1: Thu thap du lieu
    const {
        reqFullname,
        reqAddress,
        reqEmail,
        reqPhone
    } = req.body;

    // B2: Validate du lieu

    if (!reqFullname) {
        return res.status(400).json({
            message: "Full name khong hop le"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newUser = {
            fullName: reqFullname,
            email: reqEmail,
            address: reqAddress,
            phone:reqPhone
        }
        const result = await userModel.create(newUser);
        return res.status(201).json({
            message: "Tao user thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra"

        })
    }
}

const getAllUsers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await userModel.find();

        return res.status(200).json({
            message: "Lay danh sach user thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userid = req.params.userid;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findById(userid);

        return res.status(200).json({
            message: "Lay thong tin user thanh cong",
            data: result
        })
    } catch (error) {
    return res.status(500).json({
        message: "Co loi xay ra"
    })
}
}


const updateUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userid = req.params.userid;
    const {
        reqFullname,
        reqAddress
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User ID khong hop le"
        })
    }

    if (reqFullname==" ") {
        return res.status(400).json({
            message: "Full name khong hop le"
        })
    }
    if (reqAddress==" ") {
        return res.status(400).json({
            message: "Address khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateUser = {};
        if (reqFullname) {
            newUpdateUser.fullName = reqFullname
        }
        if (reqAddress) {
            newUpdateUser.address = reqAddress
        }

        const result = await userModel.findByIdAndUpdate(userid, newUpdateUser);

        if (result) {
            return res.status(200).json({
                message: "Update thong tin user thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userid = req.params.userid;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findByIdAndRemove(userid);

        if (result) {
            return res.status(200).json({
                message: "Xoa thong tin user thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


module.exports = { createUser, getAllUsers, getUserById, updateUserById, deleteUserById };

